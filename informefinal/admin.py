# -*- encoding: utf-8 -*-

from informefinal.models import InformeFinalDePasantia
from django.contrib import admin
from applabel import rename

rename("informefinal", u"Informe final de pasantía")

class InformeAdmin(admin.ModelAdmin):
    #Campos a mostrar en el listado general
    list_display = ('Expediente', 'cedula_pasante', 'titulo_informe', 'fecha_entrega')
    #Campos indexados para la busqueda
    search_fields = ['Expediente']
    # Lista 50 registro por pagina
    list_per_page = 50
    # Campos para filtrar el listado general
    list_filter = ['lapso']

admin.site.register(InformeFinalDePasantia,InformeAdmin)

