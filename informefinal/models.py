# -*- encoding: utf-8 -*-

from django.db import models
from pasantias.models import DatosDelPasante
from sisgecop.utils import string_with_title, LAPSO

class InformeFinalDePasantia(models.Model):

    # Clase del modelo del Informe final de pasantía
    Expediente= models.CharField('N° de Expediente', primary_key=True, unique=True, max_length=11)
    titulo_informe = models.CharField('Titulo del Informe de Pasantía', help_text='Escriba el titulo del informe final', max_length=200)
    fecha_entrega = models.DateField('Fecha de Entrega')
    cedula_pasante = models.ForeignKey(DatosDelPasante, verbose_name=u"Pasante", unique=True)
    lapso = models.CharField(choices=LAPSO, help_text='Ingrese el Semestre en curso del Pasante', max_length=8)

    def __unicode__(self):
        return self.titulo_informe
    
    class Meta:
        # 'informefinal' es el nombre de la aplicación django
        app_label = string_with_title("informefinal", u"Informe final de pasantía")
        verbose_name = "Informe de Pasantía"
        verbose_name_plural = "Informes de Pasantía"
