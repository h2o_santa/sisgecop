# -*- encoding: utf-8 -*-


# Vocabulario de Escuelas
ESCUELA = (
    ('41', 'Arquitectura'),
    ('42', 'Civil'),
    ('43', 'Electricidad'),
    ('44', 'Electrónica'),
    ('45', 'Industrial'),
    ('46', 'Mtto. Mecánico'),
    ('47', 'Sistemas'),
)

# Vocabulario de Semestres
SEMESTRE = (
        ('I', 'Primer'),
        ('II', 'Segundo'),
        ('III', 'Tercer'),
        ('IV', 'Cuarto'),
        ('V', 'Quinto'),
        ('VI', 'Sexto'),
        ('VII', 'Séptimo'),
        ('VIII', 'Octavo'),
        ('IX', 'Noveno'),
        ('X', 'Décimo'),
    )

# Vocabulario de Lapsos
LAPSO = (
        ('2012-1', '2012-1'),
        ('2012-2', '2012-2'),
        ('2013-1', '2013-1'),
        ('2013-2', '2013-2'),
    )


class string_with_title(str):
    ''' Clase que permite personalizar los nombres de las aplicaciones 
    en lenguaje humano en la interfaz administrativa Django '''
    
    def __new__(cls, value, title):
        instance = str.__new__(cls, value)
        instance._title = title
        return instance

    def title(self):
        return self._title

    __copy__ = lambda self: self
    __deepcopy__ = lambda self, memodict: self
