# -*- encoding: utf-8 -*-

from django.db import models
from sisgecop.utils import ESCUELA, SEMESTRE, LAPSO

class DatosDelPasante(models.Model):

    GENERO = (
        ('M', 'Masculino'),
        ('F', 'Femenino'),
    )

    nombre_pasante = models.CharField('Nombres', max_length=50, help_text='Ingrese los Nombres del Pasante')
    apellido_pasante = models.CharField('Apellidos', max_length=50, help_text='Ingrese los Apellidos del Pasante')
    cedula_pasante = models.IntegerField('Cédula de Identidad', help_text='Ingrese la Cédula del Pasante', primary_key=True, unique=True)
    fecha_nac = models.DateField('Fecha de Nacimiento')
    edad_pasante = models.IntegerField('Edad', max_length=2)
    sexo_pasante = models.CharField('Sexo', choices=GENERO, max_length=1)
    telefono_pasante = models.IntegerField('Numero Telefónico', help_text='Ejemplo= 4141234567')
    correo_pasante = models.EmailField('Correo electrónico', max_length=50, unique=True)
    direccion_pasante = models.CharField('Dirección', max_length=50, help_text='Ingrese Dirección de Residencia del Pasante')


    def __unicode__(self):
        nombre_completo = self.nombre_pasante + ' ' + self.apellido_pasante
        return unicode(nombre_completo)
    
    class Meta:
        verbose_name = "Datos del pasante"
        verbose_name_plural = "Datos de los pasantes"

class DatosDeLaOrganizacion(models.Model):
    rif_empresa = models.IntegerField('Rif', help_text='Ingrese el RIF de la Empresa/Organización', primary_key=True, unique=True)
    nombre_empresa = models.CharField('Organización', max_length=100, help_text='Ingrese el Nombre de la Organización')
    direccion_empresa = models.CharField('Dirección', max_length=200)
    telefono_empresa = models.IntegerField('Teléfono', max_length=13)
    correo_empresa = models.EmailField('Correo electrónico', max_length=150, unique=True)
    nombre_rrhh = models.CharField('Gerente de Recursos Humanos', help_text='Ingrese el Nombre del Gerente de Recursos Humanos o su Equivalente', max_length=25)


    def __unicode__(self):
        return unicode(self.nombre_empresa)
    
    class Meta:
        verbose_name = "Organización"
        verbose_name_plural = "Organizaciones"

class TutorEmpresarial(models.Model):
    nombre_tutoremp = models.CharField('Nombres', max_length= 50, help_text='Ingrese el Nombre del Tutor Empresarial')
    apellido_tutoremp = models.CharField('Apellidos', max_length=50, help_text='Ingrese el Apellido del Tutor Empresarial')
    cedula_tutoremp = models.IntegerField('Cédula de Identidad', primary_key=True, max_length=8, unique=True, help_text='Ejemplo:12543678')
    tlf_tutoremp = models.IntegerField('Numero Telefónico', help_text='Ejemplo= 4140962590 ')
    correo_tutoremp = models.EmailField('Correo Electrónico', max_length=50, help_text='Ejemplo:tutorempresarial@gmail.com')


    def __unicode__(self):
        tutor_empresarial = self.nombre_tutoremp + ' ' + self.apellido_tutoremp
        return unicode(tutor_empresarial)
    
    class Meta:
        verbose_name = "Tutor Empresarial"
        verbose_name_plural = "Tutores Empresarial"

class TutorAcademico(models.Model):
    nombre_tutoraca = models.CharField('Nombres', max_length= 50, help_text='Ingrese el Nombre del Tutor Académico')
    apellido_tutoraca = models.CharField('Apellidos', max_length=50, help_text='Ingrese el Apellido del Tutor Académico')
    cedula_tutoraca = models.IntegerField('Cédula de Identidad', primary_key=True, max_length=8, unique=True, help_text='Ejemplo:12543678')
    tlf_tutoraca = models.IntegerField('Numero Telefónico', help_text='Ejemplo= 4140962590')
    correo_tutoraca = models.EmailField('Correo Electrónico', max_length=50, help_text='Ejemplo:tutoracademico@gmail.com')


    def __unicode__(self):
        tutor_academico = self.nombre_tutoraca + ' ' + self.apellido_tutoraca
        return unicode(tutor_academico)
    
    class Meta:
        verbose_name = "Tutor Académico"
        verbose_name_plural = "Tutores Académico"

class EjecucionDePasantia(models.Model):
    Id = models.CharField('N° de Expediente', primary_key=True, unique=True, max_length=11)
    cedula_pasante = models.ForeignKey(DatosDelPasante, verbose_name=u"Pasante", unique=True)
    rif_empresa = models.ForeignKey(DatosDeLaOrganizacion, verbose_name=u"Organización")
    dpto = models.CharField('Dependencia/Departamento', help_text='Ingrese el Departamento o Dependencia donde ejecutara la Pasantía', max_length=25)
    cedula_tutoremp = models.ForeignKey(TutorEmpresarial, verbose_name=u"Tutor Empresarial")
    cedula_tutoraca = models.ForeignKey(TutorAcademico, verbose_name=u"Tutor Académico")
    fecha_inicio = models.DateField('Fecha Inicio', help_text='Ingrese la fecha de inicio de la fase ejecución')
    fecha_fin = models.DateField('Fecha Final', help_text='Ingrese la fecha final de la fase ejecución')
    escuela = models.CharField('Escuela', choices=ESCUELA, max_length=2)
    semestre = models.CharField('Semestre', choices=SEMESTRE, max_length=4, help_text='Ingrese el Semestre en curso del Pasante')
    lapso = models.CharField('Lapso', choices=LAPSO, max_length=8, help_text='Ingrese el Lapso en Curso')


    def __unicode__(self):
        return unicode(self.cedula_pasante)
    
    class Meta:
        verbose_name = "Ejecución de pasantía"
        verbose_name_plural = "Ejecuciones de pasantía"
