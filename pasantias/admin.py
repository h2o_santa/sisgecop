# -*- encoding: utf-8 -*-

from pasantias.models import DatosDelPasante, DatosDeLaOrganizacion, TutorEmpresarial, TutorAcademico, EjecucionDePasantia
from django.contrib import admin
from applabel import rename

rename("pasantias", u"Pasantias")

class PasanteAdmin(admin.ModelAdmin):
    # Campos a mostrar en el listado general
    list_display = ('cedula_pasante', 'nombre_pasante', 'apellido_pasante', 'telefono_pasante')
    # Lista 50 registro por pagina
    list_per_page = 50
    # Campos indexado para la busqueda
    search_fields = ('cedula_pasante', 'nombre_pasante') # TODO Hacer que busque por la descripcion de la escuela
    # Campos por las cuales se ordena el listado general
    ordering = ['cedula_pasante']
    #Campos para filtrar en el listado general
    list_filter = ['sexo_pasante']

admin.site.register(DatosDelPasante, PasanteAdmin)

class OrganizacionAdmin(admin.ModelAdmin):
    #Campos a mostrar en el listado general
    list_display = ('rif_empresa', 'nombre_empresa', 'nombre_rrhh', 'telefono_empresa', 'correo_empresa')
    #Campos indexados para la busqueda
    search_fields = ('rif_empresa', 'nombre_empresa')
    # Lista 50 registro por pagina
    list_per_page = 50
    #Campos por lo cual se ordena el list-ado general
    ordering = ['rif_empresa']
    # Campos para filtrar el listado general
    list_filter = ['nombre_empresa']

admin.site.register(DatosDeLaOrganizacion, OrganizacionAdmin)

class TutorEmpAdmin(admin.ModelAdmin):
    #Campos a mostrar en el listado general
    list_display = ('cedula_tutoremp', 'nombre_tutoremp', 'apellido_tutoremp', 'tlf_tutoremp')
    #Campos indexado para la busqueda
    search_fields = ('cedula_tutoremp', 'nombre_tutoremp', 'apellido_tutoremp')
    # Lista 50 registro por pagina
    list_per_page = 50
    #Campos por lo cuales se ordena el listado general
    ordering = ['cedula_tutoremp']

admin.site.register(TutorEmpresarial, TutorEmpAdmin)

class TutorAcaAdmin(admin.ModelAdmin):
    #Campos a mostrar en el listado general
    list_display = ('cedula_tutoraca', 'nombre_tutoraca', 'apellido_tutoraca', 'tlf_tutoraca')
    #Campos indexados para la busqueda
    search_fields = ('cedula_tutoraca', 'nombre_tutoraca', 'apellido_tutoraca')
    # Lista 50 registro por pagina
    list_per_page = 50
    #campos por lo cuales se ordena el listado general
    ordering = ['cedula_tutoraca']

admin.site.register(TutorAcademico, TutorAcaAdmin)

class EjecucionAdmin(admin.ModelAdmin):
    #Campos a mostrar en el listado general
    list_display = ('Id','rif_empresa', 'cedula_pasante', 'cedula_tutoremp', 'cedula_tutoraca', 'fecha_inicio', 'fecha_fin', 'lapso', 'escuela')
    #Campos para filtrar en el listado general
    list_filter = ['lapso', 'escuela']
    #Campos indexados para la busqueda
    search_fields = ['Id']
    #campos por lo cuales se ordena el listado general
    ordering = ['Id']


admin.site.register(EjecucionDePasantia, EjecucionAdmin)
