# -*- encoding: utf-8 -*-

from django.utils import timezone
#from django.template import defaultfilters
from django.db import models
from sisgecop.utils import string_with_title, ESCUELA

class GeneracionDeCarta(models.Model):

    nombre_alumno = models.CharField('Nombres', max_length=25, help_text='Ingrese los nombre del alumno que solicita la carta')
    apellido_alumno = models.CharField('Apellidos', max_length=25, help_text='Ingrese los apellidos del alumno que solicita la carta')
    cedula_alumno = models.IntegerField('Cédula de Identidad', help_text='Ingrese el numero de cédula del alumno', max_length=8, unique=True)
    escuela = models.CharField('Escuela', choices=ESCUELA, max_length=2, help_text='Ingrese la Escuela que cursa el alumno')
    nombre_organizacion = models.CharField('Organización', max_length=25, help_text='Ingrese el nombre de la organización')
    dpto_rrhh = models.CharField('A quien va dirigida', help_text='Ingrese el nombre del Jefe de Recursos quien recibirá la carta', max_length=20)
    fecha = models.DateField()

    def __unicode__(self):
        fecha_generada = timezone.localtime(self.fecha)
        return unicode(self.fecha.strftime("%d de %B de %Y") + ' ' + self.nombre_alumno)
#        return unicode(defaultfilters.date(self.fecha, 'g:i') + ' ' + self.nombre_alumno)
    
    class Meta:
        # 'cartadepostulacion' es el nombre de la aplicación Django
        app_label = string_with_title("cartadepostulacion", u"Generación de Carta")
        verbose_name = "Carta de postulación a pasantía"
        verbose_name_plural = "Cartas de postulaciones a pasantía"
