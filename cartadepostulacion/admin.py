# -*- encoding: utf-8 -*-

from cartadepostulacion.models import GeneracionDeCarta
from django.contrib import admin
from applabel import rename

rename("cartadepostulacion", u"Carta de postulación")

class CartaDePostulacionAdmin(admin.ModelAdmin):
    #Campos a mostrar en el listado general
    list_display = ('cedula_alumno', 'nombre_alumno', 'nombre_organizacion', 'fecha', 'escuela')
    #Campos para filtrar en el listado general
    list_filter = ['escuela', 'nombre_organizacion']
    #Campos indexados para la busqueda
    search_fields = ('cedula_alumno', 'nombre_alumno',)
    # Lista 50 registro por pagina
    list_per_page = 50

admin.site.register(GeneracionDeCarta,CartaDePostulacionAdmin)
